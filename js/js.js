//CONTAINER1

var bar = new ProgressBar.Circle(container, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.90);  // Number from 0.0 to 1.0

//CONTAINER2

var bar = new ProgressBar.Circle(container2, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.80);  // Number from 0.0 to 1.0

//CONTAINER3

var bar = new ProgressBar.Circle(container3, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.30);  // Number from 0.0 to 1.0

//CONTAINER4

var bar = new ProgressBar.Circle(container4, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.30);  // Number from 0.0 to 1.0

//CONTAINER5

var bar = new ProgressBar.Circle(container5, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.10);  // Number from 0.0 to 1.0

//CONTAINER6

var bar = new ProgressBar.Circle(container6, {
color: '#aaa',
// This has to be the same size as the maximum width to
// prevent clipping
strokeWidth: 4,
trailWidth: 1,
easing: 'easeInOut',
duration: 1400,
text: {
  autoStyleContainer: false
},
from: { color: '#aaa', width: 1 },
to: { color: '#fefefe', width: 4 },
// Set default step function for all animate calls
step: function(state, circle) {
  circle.path.setAttribute('stroke', state.color);
  circle.path.setAttribute('stroke-width', state.width);

  var value = Math.round(circle.value() * 100);
  if (value === 0) {
    circle.setText('');
  } else {
    circle.setText(value);
  }

}
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.10);  // Number from 0.0 to 1.0
